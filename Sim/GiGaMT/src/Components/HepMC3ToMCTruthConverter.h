/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "COPYING".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from Gaudi
#include "G4SystemOfUnits.hh"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "GiGaMT/IGiGaMTSvc.h"
#include "GiGaMTTruth/IHepMC3ToMCTruthConverter.h"
#include "HepMC3/GenParticle.h"

class G4PrimaryParticle;
class G4PrimaryVertex;
namespace LHCb {
  class IParticlePropertySvc;
}

/** @class HepMC3ToMCTruthConverter HepMC3ToMCTruthConverter.h "HepMC3ToMCTruthConverter.h"
 *
 *  Tool to loop over the HepMC3 structure and fill an MCTruthConverter object that hols
 *  the information on which particles to keep and which ones are supposed to be treated
 *  by Geant4
 *
 *  @author Dominik Muller
 *  @date   15.2.2019
 *
 */
class HepMC3ToMCTruthConverter : public extends<GaudiTool, IHepMC3ToMCTruthConverter> {
public:
  Gaudi::Property<double> m_travelLimit{ this, "TravelLimit", 1e-10 * m };
  Gaudi::Property<bool>   m_check_particle{ this, "CheckParticle", true };
  using extends::extends;

  virtual ~HepMC3ToMCTruthConverter() = default;

  virtual Gaussino::MCTruthConverterPtrs BuildConverter( const HepMC3::GenEventPtrs& ) const override;
  virtual Gaussino::MCTruthConverterPtr  BuildConverter( const HepMC3::ConstGenParticlePtr& ) const override;

private:
  ServiceHandle<LHCb::IParticlePropertySvc> m_ppSvc{ this, "PropertyService", "LHCb::ParticlePropertySvc" };
  bool                                      IsTraveling( const HepMC3::ConstGenParticlePtr& part ) const;
  /// Decide if a particle has to be kept or not.
  bool                     keep( const HepMC3::ConstGenParticlePtr& particle ) const;
  Gaussino::ConversionType GetConversionType( const HepMC3::ConstGenParticlePtr& particle ) const;
};
