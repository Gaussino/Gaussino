/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "COPYING".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GiGaMTDetectorConstructionFAC.h"
#include "G4LogicalVolumeStore.hh"
#include "GiGaMTCoreDet/GiGaMTDetectorConstruction.h"
#include "GiGaMTCoreDet/IExternalDetectorEmbedder.h"
#include "GiGaMTGeo/IGDMLReader.h"
#include "GiGaMTGeo/IGiGaMTGeoSvc.h"
#include "SimInterfaces/IGaussinoTool.h"
#include <filesystem>
DECLARE_COMPONENT( GiGaMTDetectorConstructionFAC )

StatusCode GiGaMTDetectorConstructionFAC::initialize() {
  return extends::initialize().andThen( [&]() -> StatusCode {
    StatusCode sc = StatusCode::SUCCESS;

    // Retrieve the factory tools here to avoid the retrieval happening in multiple
    // threads
    for ( auto& keypairs : m_sens_dets ) { sc &= keypairs.second.retrieve(); }
    for ( auto& embedder : m_ext_dets ) { sc &= embedder.retrieve(); }
    for ( auto& par_world : m_par_worlds ) { sc &= par_world.retrieve(); }
    for ( auto& fac : m_cust_region_factories ) { sc &= fac.retrieve(); }
    for ( auto& fac : m_cust_model_factories ) { sc &= fac.retrieve(); }

    if ( !m_outfile.value().empty() && std::filesystem::exists( m_outfile.value() ) ) {
      warning() << "GDML file " << m_outfile.value() << " already exists! "
                << "G4 will abort execution if the file GDML already exists." << endmsg;
      if ( m_outfileOverwrite.value() ) {
        warning() << "Removing the GDML file: " << m_outfile.value() << endmsg;
        std::filesystem::remove( m_outfile.value() );
      } else {
        error() << "Overwriting the GDML is disabled." << endmsg;
        return StatusCode::FAILURE;
      }
    }

    return sc;
  } );
}

StatusCode GiGaMTDetectorConstructionFAC::finalize() {
  return extends::finalize().andThen( [&]() -> StatusCode {
    if ( !m_outfile.value().empty() ) { return SaveGDML(); }
    return StatusCode::SUCCESS;
  } );
}

G4VUserDetectorConstruction* GiGaMTDetectorConstructionFAC::construct() const {
  auto detconst = new GiGaMTDetectorConstruction();
  detconst->SetWorldConstructor( [&]() {
    // Import external materials
    debug() << "Setting up external materials" << endmsg;
    for ( auto& material : m_ext_mats ) { material->construct(); }

    debug() << "Calling world constructor" << endmsg;
    auto world = m_geoSvc->constructWorld();
    for ( auto& tool : m_afterGeo ) { tool->process().ignore(); }

    // Import GDML geometry
    debug() << "Setting up volumes from GDML in mass geometry" << endmsg;
    for ( auto& reader : m_gdml_readers ) {
      if ( reader->import( world ).isFailure() ) {
        throw GaudiException( "Failed to import the GDML geometry", "GDMLReader", StatusCode::FAILURE );
      }
    }

    // Import external geometry
    debug() << "Setting up external embedder volumes in mass geometry" << endmsg;
    for ( auto& embedder : m_ext_dets ) {
      if ( embedder->embed( world ).isFailure() ) {
        throw GaudiException( "Failed to embed external geometry", "ExternalGeometry", StatusCode::FAILURE );
      }
    }

    return world;
  } );

  detconst->SetSDConstructor( [&]() {
    debug() << "Calling SD and Field constructor" << endmsg;
    m_geoSvc->constructSDandField();

    // Import external SD
    debug() << "Setting up external embedder SD in mass geometry" << endmsg;
    for ( auto& embedder : m_ext_dets ) {
      if ( embedder->embedSD().isFailure() ) {
        throw GaudiException( "Failed to embed external sensitive detectors", "ExternalGeometry", StatusCode::FAILURE );
      }
    }

    if ( DressVolumes().isFailure() ) {
      throw GaudiException( "Failed to attach sensitive detector classes", "DressVolumes", StatusCode::FAILURE );
    }

    // import custom simulation regions
    for ( auto& cust_region_factory : m_cust_region_factories ) {
      debug() << "Calling fast region constructor " << cust_region_factory->name() << endmsg;
      auto region = cust_region_factory->construct();
      if ( !region ) {
        throw GaudiException( "Failed to create the custom simulation region: " + cust_region_factory->name(),
                              "GiGaMTDetectorConstructionFAC", StatusCode::FAILURE );
      }
    }

    // import custom simulation models
    for ( auto& cust_model_factory : m_cust_model_factories ) {
      debug() << "Calling fast model constructor " << cust_model_factory->name() << endmsg;
      if ( !cust_model_factory->construct() ) {
        throw GaudiException( "Failed to create the custom simulation model: " + cust_model_factory->name(),
                              "GiGaMTDetectorConstructionFAC", StatusCode::FAILURE );
      };
    }
  } );

  // Setup parallel worlds
  for ( auto& par_world_fac : m_par_worlds ) {
    debug() << "Setting up parallel world " << par_world_fac->name() << endmsg;
    auto par_world = par_world_fac->construct();
    detconst->RegisterParallelWorld( par_world );
  }

  return detconst;
}

#include "G4LogicalVolumeStore.hh"
#include "G4SDManager.hh"

StatusCode GiGaMTDetectorConstructionFAC::DressVolumes() const {
  auto sdmanager = G4SDManager::GetSDMpointer();
  for ( auto& [name, volumes] : m_namemap ) {
    auto& tool    = m_sens_dets.at( name );
    auto  sensdet = tool->construct();
    sdmanager->AddNewDetector( sensdet );
    for ( auto& volname : volumes ) {
      auto vol = G4LogicalVolumeStore::GetInstance()->GetVolume( volname );
      if ( vol ) {
        debug() << "Attaching " << name << " to " << volname << endmsg;
        vol->SetSensitiveDetector( sensdet );
      } else {
        error() << "Couldn't find " << volname << endmsg;
        return StatusCode::FAILURE;
      }
    }
  }
  return StatusCode::SUCCESS;
}

#include "G4GDMLParser.hh"

StatusCode GiGaMTDetectorConstructionFAC::SaveGDML() const {
  try {
    G4GDMLParser g4writer;
    g4writer.SetSDExport( m_exportSD.value() );
    g4writer.SetEnergyCutsExport( m_exportEnergyCuts.value() );
    G4LogicalVolume* root = nullptr;
    if ( !m_rootVolumeName.value().empty() ) {
      auto vol_store = G4LogicalVolumeStore::GetInstance();
      if ( !vol_store ) {
        error() << "G4LogicalVolumeStore points to NULL" << endmsg;
        return StatusCode::FAILURE;
      }
      root = vol_store->GetVolume( m_rootVolumeName.value() );
      if ( !root ) {
        error() << "Cannot find " << m_rootVolumeName << "in the volume store!" << endmsg;
        return StatusCode::FAILURE;
      }
    }
    if ( !m_schema.value().empty() ) {
      g4writer.Write( m_outfile.value(), root, m_refs.value(), m_schema.value() );
    } else {
      g4writer.Write( m_outfile.value(), root, m_refs.value() );
    }
  } catch ( std::exception& err ) {
    error() << "Caught an exception while writing a GDML file: " << err.what() << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}
