Gaussino Contribution Guide
===========================

The Gaussino project is open source (see the LICENSE) and welcomes
contributions from experiments and users.

To contribute to Gaussino development you can *fork* the project from
CERN GitLab, where the Gaussino source is held.

Bug fixes and features should be developed in your own branch, then
a *merge request* made back to the upstream Gaussino repository.

By submitting a merge request to the Gaussino repository you agree that
your contribution will be copyrighted to CERN and covered by the Apache 2
licence, as per the LICENSE.

Coding Style Guidelines
-----------------------

Code contributions to Gaussino should adhere to the ``clang-format`` rules
(an automatic check is part of the Continuous Integration system).
