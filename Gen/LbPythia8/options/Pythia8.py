###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  #
#                                                                             #
# This software is distributed under the terms of the Apache License          #
# version 2 (Apache-2.0), copied verbatim in the file "COPYING".              #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    Generation,
    Inclusive,
    MinimumBias,
    Pythia8Production,
    SignalPlain,
    SignalRepeatedHadronization,
)

# from Configurables import Special

Pythia8TurnOffFragmentation = ["HadronLevel:all = off"]

gen = Generation("Generation")

gen.addTool(MinimumBias, name="MinimumBias")
gen.MinimumBias.ProductionTool = "Pythia8Production"
gen.MinimumBias.addTool(Pythia8Production, name="Pythia8Production")
gen.MinimumBias.Pythia8Production.Tuning = "LHCbDefault.cmd"

gen.addTool(Inclusive, name="Inclusive")
gen.Inclusive.ProductionTool = "Pythia8Production"
gen.Inclusive.addTool(Pythia8Production, name="Pythia8Production")
gen.Inclusive.Pythia8Production.Tuning = "LHCbDefault.cmd"

gen.addTool(SignalPlain, name="SignalPlain")
gen.SignalPlain.ProductionTool = "Pythia8Production"
gen.SignalPlain.addTool(Pythia8Production, name="Pythia8Production")
gen.SignalPlain.Pythia8Production.Tuning = "LHCbDefault.cmd"

gen.addTool(SignalRepeatedHadronization, name="SignalRepeatedHadronization")
gen.SignalRepeatedHadronization.ProductionTool = "Pythia8Production"
gen.SignalRepeatedHadronization.addTool(Pythia8Production, name="Pythia8Production")
gen.SignalRepeatedHadronization.Pythia8Production.Tuning = "LHCbDefault.cmd"
gen.SignalRepeatedHadronization.Pythia8Production.Commands += (
    Pythia8TurnOffFragmentation
)

# gen.addTool( Special , name = "Special" )
# gen.Special.ProductionTool = "Pythia8Production"
# gen.Special.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.Special.Pythia8Production.Tuning = "LHCbDefault.cmd"
# gen.Special.PileUpProductionTool = "Pythia8Production/Pythia8PileUp"
# gen.Special.addTool(Pythia8Production, name = "Pythia8PileUp")
# gen.Special.Pythia8PileUp.Tuning = "LHCbDefault.cmd"
# gen.Special.ReinitializePileUpGenerator  = False

# Use same generator and configuration for spillover
from Configurables import Gauss

spillOverList = Gauss().getProp("SpilloverPaths")
for slot in spillOverList:
    genSlot = Generation("Generation" + slot)
    genSlot.addTool(MinimumBias, name="MinimumBias")
    genSlot.MinimumBias.ProductionTool = "Pythia8Production"
    genSlot.MinimumBias.addTool(Pythia8Production, name="Pythia8Production")
    genSlot.MinimumBias.Pythia8Production.Tuning = "LHCbDefault.cmd"
