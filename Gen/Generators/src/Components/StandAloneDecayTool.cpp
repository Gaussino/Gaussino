/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "COPYING".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: StandAloneDecayTool.cpp,v 1.4 2008-07-24 22:06:07 robbep Exp $
// Include files
#include "StandAloneDecayTool.h"

// from Gaudi
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// from Generators
#include "GenInterfaces/IDecayTool.h"

#include "Defaults/HepMCAttributes.h"
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#include "HepMCUser/Status.h"
#include "HepMCUser/VertexAttribute.h"

#include "CLHEP/Random/RandFlat.h"
#include "CLHEP/Random/RandomEngine.h"

//-----------------------------------------------------------------------------
// Implementation file for class : StandAloneDecayTool
//
// 2006-04-18 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the tool factory

DECLARE_COMPONENT( StandAloneDecayTool )

//=============================================================================
// const constructor, initializes variables
//=============================================================================
StandAloneDecayTool::StandAloneDecayTool( const std::string& type, const std::string& name, const IInterface* parent )
    : Signal( type, name, parent ), m_signalMass( 0. ) {
  declareProperty( "Inclusive", m_inclusive = false );
}
//=============================================================================
// Destructor
//=============================================================================
StandAloneDecayTool::~StandAloneDecayTool() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode StandAloneDecayTool::initialize() {
  StatusCode sc = Signal::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;      // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  LHCb::IParticlePropertySvc*   ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc" );
  const LHCb::ParticleProperty* prop  = ppSvc->find( LHCb::ParticleID( *m_pids.begin() ) );
  m_signalMass                        = prop->mass();

  release( ppSvc ).ignore();

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
bool StandAloneDecayTool::generate( const unsigned int nPileUp, HepMC3::GenEventPtrs& theEvents,
                                    LHCb::GenCollisions& theCollisions, HepRandomEnginePtr& engine ) const {
  // prepare event
  LHCb::GenCollision* theGenCollision( 0 );
  HepMC3::GenEventPtr theGenEvent( 0 );

  // generate the requested number of "pile-up" events
  for ( unsigned int i = 0; i < nPileUp; ++i ) {
    prepareInteraction( &theEvents, &theCollisions, theGenEvent, theGenCollision );

    // Particle to decay
    HepMC3::GenParticlePtr theParticle{ new HepMC3::GenParticle() };
    theParticle->set_momentum( HepMC3::FourVector( 0., 0., 0., m_signalMass ) );

    // Decay the particle at (0,0,0,0)
    HepMC3::GenVertexPtr theVertex{ new HepMC3::GenVertex( HepMC3::FourVector( 0., 0., 0., 0. ) ) };
    theGenEvent->add_vertex( theVertex );
    theVertex->add_particle_out( theParticle );

    bool flip( false );

    CLHEP::RandFlat flatGenerator{ engine.getref(), 0, 1 };

    int thePID = *m_pids.begin();
    if ( m_cpMixture ) {
      // decide the PID to generate
      double flavour = flatGenerator();
      m_decayTool->enableFlip();

      if ( flavour < 0.5 )
        theParticle->set_pdg_id( +abs( thePID ) );
      else
        theParticle->set_pdg_id( -abs( thePID ) );
    } else {
      m_decayTool->disableFlip();
      theParticle->set_pdg_id( thePID );
    }

    if ( !m_inclusive )
      m_decayTool->generateSignalDecay( theParticle, flip, engine ).ignore();
    else
      m_decayTool->generateDecay( theParticle, engine ).ignore();

    theParticle->set_status( HepMC3::Status::SignalInLabFrame );

    theGenEvent->add_attribute( Gaussino::HepMC::Attributes::SignalProcessVertex,
                                std::make_shared<HepMC3::VertexAttribute>( theParticle->end_vertex() ) );
    theGenCollision->setIsSignal( true );
  }

  return true;
}

//=============================================================================
