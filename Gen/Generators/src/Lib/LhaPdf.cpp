/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "COPYING".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: LhaPdf.cpp,v 1.1 2005-12-07 23:02:09 robbep Exp $
// Include files

// local
#include "Generators/LhaPdf.h"

//-----------------------------------------------------------------------------
// Implementation file for class : LhaPdf
//
// 2005-12-07 : Patrick Robbe
//-----------------------------------------------------------------------------

Lhacontrol LhaPdf::s_lhacontrol;
