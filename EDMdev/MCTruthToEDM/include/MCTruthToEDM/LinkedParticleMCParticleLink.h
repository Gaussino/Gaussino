/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "COPYING".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <map>

class LinkedParticle;
namespace LHCb {
  class MCParticle;
}

// Typedef might be enough, let's see whether something more complicated is needed
typedef std::map<const LinkedParticle*, const LHCb::MCParticle*> LinkedParticleMCParticleLinks;
